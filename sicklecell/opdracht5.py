#!/usr/bin/env/ python

__author__ = "Falko Schoonewille"

import sys

# script dat een willekeurig aantal aminozuur-fasta-files accepteerd en vertaald naar de meest waarschijnlijke tripletten.
# Vervolgens wordt de mRNA sequentie opgeslagen in een mRNA fasta file.

reverse_codontable = {
    "M" : "ATG", "F" : "TTT", "D" : "GAT",
    "P" : "CCT", "Q" : "CAA", "V" : "GTC",
    "C" : "TGC", "K" : "AAA", "H" : "CAC",
    "STOP" : "TAA", "I" : "ATT", "R" : "CGC",
    "Y" : "TAC", "E" : "GAG", "T" : "ACA",
    "N" : "AAC", "W" : "TGG", "A" : "GCG",
    "G" : "GGC", "S" : "TCG", "L" : "CTC",
}

# returns tuple with fasta header([0]) and sequence([1])
def split_fasta(file) :
    open_file = open(file)
    file_data = open_file.readlines()
    sequence = ""
    header = ""

    for line in file_data :
        if not line.startswith('>') :
            sequence += line
        else :
            header = line

    open_file.close()
    return (header, sequence)

# translate amino_acids to mRNA using reverse codon table
def parse_mRNA(sequence):
    trans_seq = ""
    for amino_acid in sequence:
        if amino_acid in reverse_codontable:
            trans_seq += reverse_codontable[amino_acid]

    return trans_seq

# save header and mRNA sequence in file using original filename
def save_file(filename, header, sequence) :
    output_file = open(filename, 'w')
    output_file.write(header)
    output_file.write(sequence)
    output_file.close()

filenames = sys.argv[1:]
for file in filenames:
    split_fasta = split_fasta(file)
    sequence = parse_mRNA(split_fasta[1])
    save_file("mrna." + str(file), split_fasta[0], sequence)

    print("header: {}").format(split_fasta[0])
    print("sequence: {}").format(sequence)