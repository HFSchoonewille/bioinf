#!/usr/bin/env python3

"""Genbank naar FASTA-converter programma"""

# METADATA VARIABLES
__author__ = "my name"
__version__ = "2017.1"

import sys
import re


def read_gkb(file):
    pass


def process_exon_line(line):
    pass


def clip_exons(locs, seq):
    '''
    function strip the mRNA line from a GenBank file to onbtain the exon positions
    and uses these index postions to string slice the exons the the entire DNA seq
    locs = the locations from the exons
    seq = the combined DNA sequence
    '''
    exons_seq = ''
    for loc in locs:
        [start, stop] = loc.split("..")
        exons_seq += seq[int(re.sub("\D", "", start)):int(re.sub("\D", "", stop))]
    return exons_seq


def save_file(file_name, string):
    pass


def construct_fasta_header(file_name):
    file = open(file_name, 'r')
    fasta_version = ""
    fasta_definition = ""

    definition_line = False
    for line in file:
        if line.startswith("VERSION"):
            fasta_version = line.replace("VERSION", "").strip()
        if line.startswith("DEFINITION"):
            definition_line = True
        if line.startswith("ACCESSION"):
            definition_line = False
        if definition_line:
            fasta_definition += line.replace("DEFINITION ", "|").strip()

    file.close()
    return ">" + fasta_version + " " + fasta_definition


def construct_fasta_sequence(file_name):
    file = open(file_name)
    origin_line = False
    sequence = ""

    for line in file:
        if line.startswith("ORIGIN"):
            origin_line = True

        if line.startswith("//"):
            origin_line = False

        if origin_line:
            sequence += line[9:].replace(" ", "")

    return sequence.strip("\n").upper()


def main():
    for file_name in sys.argv[1:]:
        fasta_header = construct_fasta_header(file_name)
        fasta_sequence = construct_fasta_sequence(file_name)
        print(fasta_header)
        print(fasta_sequence)

main()
