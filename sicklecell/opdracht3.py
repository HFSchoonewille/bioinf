#!#!/usr/bin/env python3

"""Dit leest een DNA of mRNA fasta file en vertaald deze naar aminozuren"""

# METADATA VARIABLES
__author__ = "my name"
__version__ = "2017.1"

# IMPORTS
import sys

def translate_dna(sequence, start=0):
    codontable = {
        'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
        'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
        'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
        'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
        'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
        'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
        'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
        'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
        'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
        'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
        'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
        'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
        'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
        'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
        'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
        'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
    }
    trans_seq = ""
    for i in range(start, len(sequence), 3):
        #slice een stukje van drie
        codon = sequence[i:i+3]
        if codon in codontable:
            trans_seq += codontable[codon]
    return trans_seq

# LOGICA
start = 0;
if (sys.argv[1].isdigit()):
    start = int(sys.argv[1])
    filenames = sys.argv[2:]

else:
    filenames = sys.argv[1:]

for fasta_file in filenames:
    f = open(fasta_file, 'r')

    # maak eerst een sequentie met alleen dna
    sequence = ""
    header = ""
    for line in f:
        if not line.startswith('>'):
            sequence = sequence + line.strip()
        else:
            header = line.strip('\n')

    if not sequence[:1] == "M":
        output_file = "out" + fasta_file + ".fasta"
        o = open(output_file, 'w')

        # vertaal nu de sequentie
        amino_acids = translate_dna(sequence, start)
        print(header)
        print(amino_acids)
        # en schrijf de sequentie weg
        o.write(amino_acids)
        f.close()
        o.close()