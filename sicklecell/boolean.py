#!/usr/bin/env python3
from random import choice

# Let's (again) first make a list of what our program should do:
# it should generate a random DNA sequence of 20 nucleotides in length
def random_sequence():
    DNA = ""
    for count in range(20):
        DNA += choice("CGTA")
    return DNA

def random_sequence_gc50():
    gc_percent = 0
    # the GC percentage should be at least 50%
    # the max GC percentage should be 60%
    while gc_percent < 50 or gc_percent > 60:
        DNA = random_sequence()
        gc_content = DNA.count('C') + DNA.count('C')
        gc_percent = gc_content * 100 / len(DNA)
    # it should print the GC percentage to the terminal
    print("GC percent: {}%").format(gc_percent)
    # it should print the sequence to the terminal
    print(DNA)

random_sequence_gc50()

