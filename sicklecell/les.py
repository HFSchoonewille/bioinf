import turtle

don = turtle.Turtle
def polygon(sides, size=100):
    for step in range(sides):
        don.forward(size)
        don.right(360/sides)
    return

polygon(25,50)