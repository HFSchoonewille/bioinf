#!/usr/bin/ python3

"""Dit programma leest fasta file(s) en rapporteert aantal nucleotide/aminozuren"""

# METADATA VARIABLES
__author__ = "Falko en Wouter"
__version__ = "2017.1"

# IMPORTS
import sys

# LOGICA
def count_ATCG(sequence):
    A = 0
    C = 0
    T = 0
    G = 0

    for char in sequence:
        if (char == "A"):
            A += 1
        if (char == "C"):
            C += 1
        if (char == "T"):
            T += 1
        if (char == "G"):
            G += 1

    return {"A": A,
            "C": C,
            "T": T,
            "G": G}

def count_cg(sequence):
    gc = sequence.count('G') + sequence.count('C')
    return gc * 100 / len(sequence)

def file_to_string(file):
    # Functie die een string opbouwt en de eerste regel van het bestand uitsluit
    file_data = open(file)
    sequence = ""

    for line in file_data:
        if not line.startswith('>'):
            sequence += line

    return sequence

def determine_type(sequence):
    file_type = ""
    if (sequence[:1] ==  "M"):
        file_type = "protein"
    else:
        file_type = "dna"

    return file_type

# adjust
filenames = sys.argv[1:]
for file in filenames:
    open_file = open(file)
    sequence = file_to_string(file)
    file_type = determine_type(sequence)
    # if protein_fasta
    if (file_type == "protein"):
        # count amino acids
        print("Aantal aminozuren: {}".format(len(sequence)))
    # if nucleotide_fasta
    if (file_type == "dna"):
        # count nucleotides
        print("Aantal nucleotiden: {}".format(len(sequence)))
        # calc number of G', number of A's, number of T's, number of C's
        ATCG_list = count_ATCG(sequence)
        for key, value in ATCG_list.items():
            print("Aantal {}: {}".format(key, value))
        # calc GC-count
        print("GC Count: {}%".format(count_cg(sequence)))

    open_file.close()
